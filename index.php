<?php 
	header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
?>

<!DOCTYPE html>
<html lang="es-Es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Evitar que se cargue el cache de la página durante el desarrollo -->
	<meta http-equiv="cache-control" content="no-store, no-cache" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0">

	<title>Sitio Web en desarrollo</title>
	
	<!-- Hojas de Estilos -->
	<link rel="stylesheet" href="css/styles.css">
	
	<!-- Fontawesome CSS -->
	<link rel="stylesheet" href="fontawesome-free-5.5.0-web/css/all.css">

	<!-- Favicon -->
	<link rel="icon" sizes="any" href="images/favicon.png">

	<!-- JavaScript -->
	<script type="text/JavaScript" src="script/jquery-3.3.1.js"></script>

	<!-- Fontawesome JavaScript -->
	<!-- <script type="text/JavaScript" src="fontawesome-free-5.5.0-web/js/all.js"></script> -->
</head>
<body>
	<main role="main" class="dl-container">
		<!-- Cabecera -->
		<header class="dluna dl-container__item">
			<!-- Barra de navegación -->
			<nav class="navigation" id="nav-bar">
				<!-- Logo -->
				<div class="images">
					<img src="images/logo.svg" alt="LUNAMONTILLA">
					<span class="color-primario">LUNAMONTILLA</span>
				</div>
				<!-- Fin Logo -->
				
				<!-- Botón Menú -->
				<input type="checkbox" id="chkMenu">
				<label for="chkMenu"><span class="fas fa-bars fa-lg"></span></label>
				<!-- Fin Botón Menú -->
				
				<!-- Menú de navegación -->
				<ul class="menu">
					<li><a href="#">Portafolio</a></li>
					<li><a href="#">Proyectos</a></li>
				</ul>
				<!-- Fin Menú de navegación -->
			</nav>

			<!-- Fin Barra de navegación -->
		</header>
		
		<!-- Contenedor principal -->
		<div class="main-container dl-container__item">
			<div class="section-container">
				<section class="dl-section">
					<h1>Sitio Web en desarrollo</h1>
					<hr>

					<h2 class="color-primario">Lo que se colocará</h2>
					<p>Pronto estará disponible en este sitio Web, portafolios y plantillas Web, además, de ejemplos de código.</p>
				</section>
			</div>

			<!-- Barra lateral -->
			<div class="sidebar-container"></div>
		</div>
		
		<!-- Pie de página -->
		<footer class="dl-footer dl-container__item">
			<p>&copy; Copyright 2018 - David E Luna M</p>
		</footer>
	</main>
</body>
</html>